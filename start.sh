#!/bin/sh

erl \
  -sname receive_server_test_node \
  -pa ../*/ebin ./deps/*/ebin \
  -config priv/app.config \
  -eval 'lists:foreach(fun(App) -> ok = application:start(App) end, [ syntax_tools, compiler, goldrush, lager, eamqp, receive_server ])' 
