#include "erl_nif.h"
#include "crc.h"

static ERL_NIF_TERM crc_itu_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
	ErlNifBinary binary;
	int dataSize = 0;
	unsigned char* data = NULL;
	unsigned short crc_itu = 0;

	if(!enif_inspect_binary(env, argv[0], &binary)) {
		return enif_make_badarg(env);
	}

	dataSize = binary.size;
	data = binary.data;
	crc_itu = CRC_ITU(data, dataSize);

	return enif_make_int(env, crc_itu);
}

static ErlNifFunc nif_funcs[] = {
    {"crc_itu", 1, crc_itu_nif}
};

ERL_NIF_INIT(crc, nif_funcs, NULL, NULL, NULL, NULL)
