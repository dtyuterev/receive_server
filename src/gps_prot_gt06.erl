-module(gps_prot_gt06).

-define(StartBit, <<16#78,16#78>> ).
-define(EndBit, <<16#0d,16#0a>> ).
-record(dr, {pl,pn,ic,isn,ec}).
%-record(parsed_data,{}).
-define(MSG_TYPES,[
    {16#01, login},     	% Login Message
    {16#10, gps_data},  	% GPS Data
    {16#11, lbs_data},  	% LBS Data
    {16#12, gps_lbs_data},  	% GPS/LBS Data
    {16#13, status},    	% Status Information
    {16#14, satellite},    	% Satellite SNR message
    {16#15, info},      	% String Information
    {16#16, gps_lbs_status},   	% GPS/LBS/Status data
    {16#17, lbs_query},  	% LBS, Query address Data by phone number
    {16#18, ext_lbs},	  	% Extended LBS Data
    {16#19, lbs_status},   	% LBS/Status data
    {16#1A, gps_query},       	% GPS, Query address Data by phone number
    {16#80, s_command},    	% Command information srv->term (setting)
    {16#81, q_command}    	% Command information srv->term (quering)
]).

-export([parse_msg/1,find_c/1, init_msg/1]).

%----------------------------------------------------------
-spec init_msg(atom) -> {ok,{response,binary()|undefined},{parsed_data,#dr{}|undefined},{state_data,list()|undefined}} | {error, term()}.
init_msg(gps_data) ->
	lager:info("Get init_msg : init_gps"),
	PN=16#81,
	Command= <<"WHERE#">>,
	COI=list_to_binary([byte_size(Command), <<1:32>>, Command]),
	ISN= <<1:16>>,
    	RespPart = list_to_binary([byte_size(list_to_binary([PN,COI,ISN]))+2, PN, COI, ISN]),
	RespChkErr = binary:encode_unsigned(sign(RespPart), big),
	Resp = list_to_binary([?StartBit,RespPart,RespChkErr,?EndBit]),
	{ok,{response,Resp},{parsed_data,undefined},{state_data,undefined}};
init_msg(Some) ->
	lager:warning("Get some strange in init_msg : ~p",[Some]),
	{ok,{response,undefined},{parsed_data,undefined},{state_data,undefined}}.
%----------------------------------------------------------
-spec parse_msg(binary()) -> {ok,{response,binary()|undefined},{parsed_data,#dr{}|undefined},{state_data,list()|undefined}} | {error, term()}.
parse_msg(Data) when is_binary(Data)->
    bth(Data),
    lager:info("Got to parse : ~p",[Data]),
    case Data of
        << 16#78,16#78, PL/integer, Tail/binary >> ->
            lager:info("Correct start bit and packet lenght (~p)",[PL]),
            PLi = PL-5,
            << PN/integer, InfCont:PLi/binary, InfSerNum:2/binary, ErrorCheck:2/binary, 16#0D,16#0A, _/binary >> = Tail,
            case checkErrSum( list_to_binary([PL, PN, InfCont, InfSerNum]), ErrorCheck ) of
                true ->
                    lager:info("Sign check success"),
                    DR = #dr{ pl = PL, pn = PN, ic = InfCont, isn = InfSerNum, ec = ErrorCheck},
                    MT = get_msg_type(DR),
                    process_msg(MT,DR);
                false ->
                    lager:info("Sign check fail"),
                    {error,"Sign check fail"}
            end;
		_ ->
			lager:info("It's not seemed like gps_data"),
			{error,"It's not gps data"}
	end;
parse_msg(Data) ->
	lager:info("It's not Binary ! ~p",[Data]),
	{error,"Wrong data"}.
%----------------------------------------------------------
checkErrSum(Packet,ErrSum) ->
    Calculated=crc:crc_itu(Packet),
    ErrSumInt = hd([ H || <<H:2/big-unsigned-integer-unit:8>> <= ErrSum ]),
    lager:info("~p (~p) vs ~p (~p)",[Calculated,Packet,ErrSumInt,ErrSum]),
    Calculated == ErrSumInt.
%----------------------------------------------------------

sign(Packet)->
    lager:info("Got command to sign packet: ~p",[Packet]),
    Sign=crc:crc_itu(Packet),
    lager:info("Sign result: ~p",[Sign]),
    Sign.
	
%----------------------------------------------------------

process_msg(login,Data) -> 
    lager:info("Proccess Login Message"),
    <<TermBin:8/binary,T/binary>> = Data#dr.ic,
    TerminalID = binary_to_integer( << <<Y>> || <<X:4>> <= TermBin, Y <- integer_to_list(X) >>),
    %It's also worked !!! TerminalID = binary:encode_unsigned(Data#dr.ic,big),
    lager:info("IMEI: ~p (~p)",[TerminalID,TermBin]),
    RespPart = list_to_binary([byte_size(list_to_binary([Data#dr.pn,Data#dr.isn]))+2, Data#dr.pn, Data#dr.isn]),
    RespChkErr = binary:encode_unsigned(sign(RespPart), big),
    lager:info("ResponseCC ~p",[RespChkErr]),
    Resp = list_to_binary([?StartBit,RespPart,RespChkErr,?EndBit]),
    lager:info("Response ~p",[Resp]),
    {ok,{response,Resp},{parsed_data,undefined},{state_data,[{imei,TerminalID}]}}; 
process_msg(status,Data) -> 
    lager:info("Proccess Status Message"),
    <<TIC:1/binary, VL/integer, GSMSigStr/integer, _AL/binary>> = Data#dr.ic,
    <<
	OE:1/big-unsigned-integer-unit:1,
	GPS:1/big-unsigned-integer-unit:1,
	Alrm:1/big-unsigned-integer-unit:3,
	Chrg:1/big-unsigned-integer-unit:1,
	ACC:1/big-unsigned-integer-unit:1,
	Defense:1/big-unsigned-integer-unit:1
    >>=TIC,
    lager:info("DATA: ~p PWR: ~p SIG: ~p (GPS:~p, ACC: ~p)",[TIC,VL,GSMSigStr,GPS, ACC]),
    RespPart = list_to_binary([byte_size(list_to_binary([Data#dr.pn,Data#dr.isn]))+2, Data#dr.pn, Data#dr.isn]),
    RespChkErr = binary:encode_unsigned(sign(RespPart), big),
    lager:info("ResponseCC ~p",[RespChkErr]),
    Resp = list_to_binary([?StartBit,RespPart,RespChkErr,?EndBit]),
    lager:info("Response ~p",[Resp]),
    {ok,{response,Resp},{parsed_data,undefined},{state_data,undefined}}; 
process_msg(ext_lbs,Data) -> 
    lager:info("Proccess ext_lbs Message"),
    <<
	Date:6/binary,
	MCC:1/big-unsigned-integer-unit:16,
	MNC:1/big-unsigned-integer-unit:8,
	LAC:1/big-unsigned-integer-unit:16,
	CellID:1/big-unsigned-integer-unit:24,
	Tail/binary
    >> = Data#dr.ic,
    <<Year/integer,Month/integer,Day/integer,Hour/integer,Minute/integer,Second/integer>>=Date,
    lager:info("Date: 20~p-~p-~p ~p:~p:~p",[Year,Month,Day,Hour,Minute,Second]),
    lager:info("MCC: ~p; MNC: ~p, LAC: ~p, CellID: ~p",[MCC,MNC,LAC,CellID]),
    RespPart = list_to_binary([byte_size(list_to_binary([Data#dr.pn,Data#dr.isn]))+2, Data#dr.pn, Data#dr.isn]),
    RespChkErr = binary:encode_unsigned(sign(RespPart), big),
    lager:info("ResponseCC ~p",[RespChkErr]),
    Resp = list_to_binary([?StartBit,RespPart,RespChkErr,?EndBit]),
    lager:info("Response ~p",[Resp]),
    {ok,{response,Resp},{parsed_data,undefined},{state_data,undefined}};
process_msg(gps_data,Data) -> 
    lager:info("Proccess GPS Location Message"),
    <<Date:6/binary, GPSInfo:1/binary,Lat:4/binary,Lng:4/binary,Speed/integer,Course:2/binary , _Reserve/binary>>=Data#dr.ic,
    <<Year/integer,Month/integer,Day/integer,Hour/integer,Minute/integer,Second/integer>>=Date,
    DateStr = io_lib:format("20~2..0w-~2..0w-~2..0w ~2..0w:~2..0w:~2..0w",[Year,Month,Day,Hour,Minute,Second]),
    lager:info("Date: 20~p-~p-~p ~p:~p:~p",[Year,Month,Day,Hour,Minute,Second]),
    LatInt = hd([ H || <<H:1/big-unsigned-integer-unit:32>> <= Lat ])/(30000*60),
    LngInt = hd([ H || <<H:1/big-unsigned-integer-unit:32>> <= Lng ])/(30000*60),
    lager:info("Lat:~p, Lng: ~p",[LatInt,LngInt]), 
    lager:info("Speed: ~p",[Speed]),
    {ok,{response,undefined},{parsed_data,[{x,LatInt},{y,LngInt},{date,{{2000+Year,Month,Day},{Hour,Minute,Second}}}]},{state_data,undefined}}; 
process_msg(Cmd,Data) -> 
    lager:info("Sorry, unknown or unreleased command : ~p",[Cmd]),
    lager:debug("~p",[Data]),
    {ok,{response,undefined},{parsed_data,undefined},{state_data,undefined}}. 

%----------------------------------------------------------
-spec get_msg_type(data()) -> {ok, msg_type()} | undefined.
-type data() :: #dr{}.
-type msg_type() :: login|info|alarm|gps|command.
get_msg_type(Data) ->
    proplists:get_value(Data#dr.pn,?MSG_TYPES,undefined).

%----------------------------------------------------------
bth(Bin) ->
	Result = [ integer_to_list(X,16) || <<X:8>> <= Bin ],
	lager:info("In hex: ~p",[Result]).

%find_c(<<A:1/binary,B:1/binary,C:1/binary,D:1/binary,TailBin/binary>>) ->
find_c(<<A:1/binary,B:1/binary,C:1/binary,D:1/binary,TailBin/binary>>) ->
	%<<14,6,7,17,46,10,0,250,2,30,129,0,86,95,77,30,129,0>>
	LB = list_to_binary([A,B,C,D]),
	%LB = list_to_binary([A,B]),
	RLB = list_to_binary([D,C,B,A]),
    	LatInt = (hd([ H || <<H:1/big-unsigned-integer-unit:32>> <= LB ])/3000)/60,
    	LngInt = hd([ H || <<H:1/big-unsigned-integer-unit:32>> <= LB ]),
	lager:info("Is it needed ?: ~p, ~p (~p)",[LatInt,LngInt,LB]),
	find_c(list_to_binary([B,C,D,TailBin]));
find_c(<<_:1/binary>>) -> ok;
find_c(<<>>) ->
	lager:info("End find coords").

%2014-06-15 19:01:46.234 [info] <0.290.0>@gps_prot_gt06:bth:125 In hex: ["78","78","19","10","E","6","F","F","1","2B","CA","6", "6B","FA","93","3","3D","F","EF","0","14","EA","0","1","0","5","62","E8","D","A"]
%2014-06-15 19:01:46.234 [info] <0.290.0>@gps_prot_gt06:parse_msg:22 Got to parse : <<120,120,25,16,14,6,15,15,1,43,202,6,107,2 50,147,3,61,15,239,0,20,234,0,1,0,5,98,232,13,10>> (HEX: ok)
