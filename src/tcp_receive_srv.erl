-module(tcp_receive_srv).

-include_lib("receive_server.hrl").

-behaviour(gen_server).
-behaviour(ranch_protocol).

%% API.
-export([start_link/4]).

%% gen_server.
-export([init/1]).
-export([init/4]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-define(TIMEOUT, infinity).


-record(state, {socket, transport, port, data}).

%% API.

start_link(Ref, Socket, Transport, Opts) ->
	proc_lib:start_link(?MODULE, init, [Ref, Socket, Transport, Opts]).

%% gen_server.

%% This function is never called. We only define it so that
%% we can use the -behaviour(gen_server) attribute.
init([]) -> {ok, undefined}.

init(Ref, Socket, Transport, [Port]) ->
    ok = proc_lib:init_ack({ok, self()}),
    ok = ranch:accept_ack(Ref),
    ok = Transport:setopts(Socket, [binary,{packet,0},{active, once}]),
    %erlang:send_after(10000,self(),init_gps),
    gen_server:enter_loop(?MODULE, [], #state{socket=Socket, transport=Transport, port=Port}, ?TIMEOUT).

handle_info({tcp, Socket, Data}, State=#state{ socket=Socket, transport=Transport}) ->
    lager:debug("Get some: ~p",[Data]),
    %_BPort=list_to_binary(integer_to_list(Port)),
    case gps_prot_gt06:parse_msg(Data) of
        {ok,{response,Response},{parsed_data,PData},{state_data, SData}} ->
            lager:info("Got OK result, continue"),
	    NewState = update_state(State,SData),
            send_response(Transport,Socket,Response),
            send_parsed_data(NewState,PData),
    	    {noreply, NewState, ?TIMEOUT};
        {error, Reason} ->
            lager:debug("Error Parse result: ~p",[Reason]),
    	    {noreply, State, ?TIMEOUT}
    end;
handle_info(init_gps, State=#state{ socket=Socket, transport=Transport}) ->
    lager:debug("Start init_gps"),
    case gps_prot_gt06:init_msg(gps_data) of
        {ok,{response,Response},{parsed_data,PData},{state_data, SData}} ->
            lager:info("Got OK result, continue"),
	    NewState = update_state(State,SData),
            send_response(Transport,Socket,Response),
            send_parsed_data(NewState,PData),
    	    erlang:send_after(3000,self(),init_gps),
    	    {noreply, NewState, ?TIMEOUT};
        {error, Reason} ->
            lager:debug("Error Parse result: ~p",[Reason]),
    	    {noreply, State, ?TIMEOUT}
    end;
handle_info({tcp_closed, _Socket}, State) ->
    {stop, normal, State};
handle_info({tcp_error, _, Reason}, State) ->
    {stop, Reason, State};
handle_info(timeout, State) ->
    {stop, normal, State};
handle_info(_Info, State) ->
    {stop, normal, State}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%---------------------------------------------------------------------------------------------------
send_response(_,_,undefined) -> 
	lager:info("Nothing sent to device"),
	ok;
send_response(T,S,Msg) -> 
	T:setopts(S, [{active, once}]),
	lager:info("Send data to device"),
	T:send(S,Msg).
%---------------------------------------------------------------------------------------------------
send_parsed_data(_,undefined)->
	lager:info("Nothing sent to mq"),
	ok;
send_parsed_data(State,Msg)->
	DataToSend = lists:append(Msg,State#state.data),
	lager:debug("Data will send : ~p",[DataToSend]),
	?SENDER:send_msg(term_to_binary(DataToSend)).
%---------------------------------------------------------------------------------------------------
update_state(State, undefined) -> 
	lager:info("Nothing sent to state"),
	State;
update_state(State, Data) -> 
	State#state{ data = Data}.

