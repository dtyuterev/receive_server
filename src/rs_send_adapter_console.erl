-module(rs_send_adapter_console).

-export([send_msg/1]).

send_msg(Bin)->
    lager:info("Got msg to send to RabbitMQ"),
    lager:debug("~p",[Bin]),
    eamqp:send_msg(Bin),
    ok.
