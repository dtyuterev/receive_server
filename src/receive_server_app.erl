-module(receive_server_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
	application:ensure_started(ranch),
    {ok,Ports} = application:get_env(receive_server,ports),
    {ok,WorkersPerPort} = application:get_env(receive_server,workers_per_port),
    lager:info("Got ~p ports (WPP:~p)to listen : ~p",[length(Ports),WorkersPerPort,Ports]),
    Result = [
       ranch:start_listener(io_lib:format("tcp_receive_~p",[X]), WorkersPerPort, ranch_tcp, [{port, X}], tcp_receive_srv, [X])
       || X <- Ports
    ],
    lager:debug("Starting result : ~p",[Result]),
   	tcp_receive_sup:start_link().

stop(_State) ->
    ok.
