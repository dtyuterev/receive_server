-module(crc).
-export([crc_itu/1]).
-on_load(init/0).

init() ->
    case code:which(crc) of
        Filename when is_list(Filename) ->
            erlang:load_nif(filename:join([filename:dirname(Filename),
                                           "..","priv",
                                           "crc_nif"]), []);
        Reason when is_atom(Reason) ->
            {error, Reason}
    end.

crc_itu(_Data) ->
  erlang:nif_error(nif_library_not_loaded).
